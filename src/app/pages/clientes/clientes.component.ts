import { Component, OnInit } from '@angular/core';
import { ClientesService } from 'src/app/services/clientes.service';
import { Cliente } from 'src/app/interfaces/cliente.interface';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[] = [];
  filtro = '';
  ordenada = 'id';
  pageSize = 5;
  pageNumber = 1;
  pageSizeOptions = [2, 4, 5, 10];

  constructor( private clientesService: ClientesService ) { }

  async ngOnInit() {
    await this.clientesService.getAll().subscribe( ( data: Cliente[] ) => {
      this.clientes = data;
    });
  }

  busqueda( value: string ) {
    this.filtro = value;
  }

  paginar( e: PageEvent ) {
    this.pageSize = e.pageSize;
    this.pageNumber = e.pageIndex + 1;
  }

}
