import { Component, OnInit } from '@angular/core';
import { Zona } from 'src/app/interfaces/zona.interface';
import { ZonasService } from 'src/app/services/zonas.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-zonas',
  templateUrl: './zonas.component.html',
  styleUrls: ['./zonas.component.css']
})
export class ZonasComponent implements OnInit {

  zonas: Zona[] = [];
  filtro: string;
  ordenada = 'id';
  pageSize = 5;
  pageNumber = 1;
  pageSizeOptions = [ 2, 4, 5, 10 ];

  constructor( private zonasService: ZonasService ) { }

  async ngOnInit() {
    await this.zonasService.getAll().subscribe( ( data: Zona[] ) => {
      this.zonas = data;
    } );
  }

  busqueda( value: string ) {
    this.filtro = value;
  }

  paginar( e: PageEvent ) {
    this.pageSize = e.pageSize;
    this.pageNumber = e.pageIndex + 1;
  }

}
