import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Sucursal } from 'src/app/interfaces/sucursal.interface';
import { SucursalesService } from 'src/app/services/sucursales.service';

@Component({
  selector: 'app-sucursales',
  templateUrl: './sucursales.component.html',
  styleUrls: ['./sucursales.component.css']
})
export class SucursalesComponent implements OnInit {

  sucursales: Sucursal[] = [];
  filtro = '';
  ordenada = 'id';
  pageSize = 5;
  pageNumber = 1;
  pageSizeOptions = [2, 4, 5, 10];

  constructor(private sucursalService: SucursalesService) { }

  async ngOnInit() {
    await this.sucursalService.getAll().subscribe((data: Sucursal[]) => {
      this.sucursales = data;
    });
  }
  busqueda(value: string) {
    this.filtro = value;
  }

  paginar(e: PageEvent) {
    this.pageSize = e.pageSize;
    this.pageNumber = e.pageIndex + 1;
  }

}
