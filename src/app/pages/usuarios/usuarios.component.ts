import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../../services/usuarios.service';
import { Usuario } from 'src/app/interfaces/usuario.interface';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  usuarios: Usuario[] = [];
  filtro = '';
  ordenada = 'id';
  pageSize = 5;
  pageNumber = 1;
  pageSizeOptions = [2, 4, 5, 10];

  constructor( private usuariosService: UsuariosService ) { }

  async ngOnInit() {
    await this.usuariosService.getAll().subscribe((data: Usuario[]) => {
      console.log(data);
      this.usuarios = data;
    });
  }

  busqueda( value: string ) {
    this.filtro = value;
  }

  paginar( e: PageEvent ) {
    this.pageSize = e.pageSize;
    this.pageNumber = e.pageIndex + 1;
  }

}
