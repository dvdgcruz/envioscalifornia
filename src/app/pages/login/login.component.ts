import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel = new UsuarioModel();
  recordarme = false;

  constructor( private auth: AuthService, private router: Router ) { }

  ngOnInit() {
    if ( localStorage.getItem('clave') ) {

      this.usuario.clave = localStorage.getItem('clave');
      this.recordarme = true;

    } else {
      localStorage.getItem('clave');
    }
  }

  login( form: NgForm ) {

    if ( form.invalid ) { return; }

    // Swal.fire({
    //   allowOutsideClick: false,
    //   icon: 'info',
    //   text: 'Espere un momento por favor'
    // });
    // Swal.showLoading();

    // TODO: mandar al auth service el usuario para intentar hacer login
    if ( this.recordarme ) {
      // tslint:disable-next-line: no-string-literal
      localStorage.setItem('clave', this.usuario['clave']);
    }

    this.router.navigateByUrl('/envios');

    // Swal.fire({
    //   icon: 'error',
    //   title: 'Error al autenticar',
    //   text: 'ERRRROOOOORRRRR'
    //   // text: err.error.error.message
    // });
  }

}
