export class UsuarioModel {
    id: string;
    nombre: string;
    apPaterno: string;
    apMaterno: string;
    nombreCompleto: string;
    clave: string;
    password: string;
    telefono: string;
    rol: string;
    sucursal: number;
    activo: boolean;
}
