import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  buscar = new FormControl('');
  // tslint:disable-next-line: no-output-rename
  @Output('buscar') buscarEmitter = new EventEmitter<string>();


  constructor() { }

  ngOnInit(): void {
    this.buscar.valueChanges
    .pipe( debounceTime(300) )
    .subscribe( value => this.buscarEmitter.emit( value ) );
  }

}
