import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() actual: string;

  constructor( private auth: AuthService, private router: Router ) { }

  ngOnInit(): void {
  }

  salir() {
    this.auth.logout();
    this.router.navigateByUrl('/login');
  }

}
