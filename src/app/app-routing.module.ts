import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { EnviosComponent } from './pages/envios/envios.component';
import { SucursalesComponent } from './pages/sucursales/sucursales.component';
import { ZonasComponent } from './pages/zonas/zonas.component';


const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'usuarios', component: UsuariosComponent },
    { path: 'clientes', component: ClientesComponent },
    { path: 'envios', component: EnviosComponent },
    { path: 'sucursales', component: SucursalesComponent },
    { path: 'zonas', component: ZonasComponent },
    { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
