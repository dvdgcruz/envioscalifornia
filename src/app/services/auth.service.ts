import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Usuario } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userToken: string;

  constructor( private http: HttpClient ) { }

  logout() {
    localStorage.removeItem('token');
  }

  login( usuario: Usuario ) {

    const authData = {
      email: usuario.clave,
      password: usuario.password,
      returnSecureToken: true
    };

  }

  leerToken() {
    localStorage.getItem('token') ? this.userToken = localStorage.getItem('token') : this.userToken = '';

    return this.userToken;
  }

  estaAutenticado(): boolean {

    if ( this.userToken.length < 2 ) { return false; }

    const expira = Number(localStorage.getItem('expira'));
    const expiraDate = new Date();
    expiraDate.setTime( expira );

    if ( expiraDate > new Date() ) {
      return true;
    } else {
      return false;
    }

  }

  private guardarToken( idToken: string ) {

    this.userToken = idToken;
    localStorage.setItem('token', idToken);

    const hoy = new Date();
    hoy.setSeconds( 3600 );

    localStorage.setItem('expira', hoy.getTime().toString() );
  }

}
