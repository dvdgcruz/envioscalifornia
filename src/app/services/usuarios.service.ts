import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private jsonURL = 'assets/data/usuarios.data.json';

  constructor( private http: HttpClient ) {}

  getAll() {
    return this.http.get<Usuario[]>( this.jsonURL );
  }
}
