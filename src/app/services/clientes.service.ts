import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cliente } from '../interfaces/cliente.interface';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  private jsonURL = 'assets/data/clientes.data.json';

  constructor( private http: HttpClient ) { }

  getAll() {
    return this.http.get<Cliente[]>( this.jsonURL );
  }
}
