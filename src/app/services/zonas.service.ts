import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Zona } from '../interfaces/zona.interface';

@Injectable({
  providedIn: 'root'
})
export class ZonasService {

  private jsonURL = 'assets/data/zonas.data.json';

  constructor( private http: HttpClient ) { }

  getAll() {
    return this.http.get<Zona[]>( this.jsonURL );
  }
}
