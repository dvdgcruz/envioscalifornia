import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Sucursal } from '../interfaces/sucursal.interface';

@Injectable({
  providedIn: 'root'
})
export class SucursalesService {

  private jsonURL = 'assets/data/sucursales.data.json';

  constructor( private http: HttpClient ) { }

  getAll() {
    return this.http.get<Sucursal[]>( this.jsonURL );
  }
}
