export interface Cliente {
    id: string;
    nombre: string;
    apPaterno: string;
    apMaterno: string;
    nombreCompleto: string;
    curp: string;
    direccion: string;
    municipio: string;
    telefono: string;
    creadoPor: number;
    tipo: number;
    fechaCreacion: number;
    activo: boolean;
}
