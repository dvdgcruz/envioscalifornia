export interface Sucursal {
    id: string;
    nombre: string;
    telefono: string;
    activo: boolean;
}
