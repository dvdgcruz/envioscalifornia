export interface Zona {
    id: string;
    nombre: string;
    clave: string;
    telefono: string;
    activo: boolean;
}
