export interface Envio {
    id: string;
    folio: string;
    fechaRegistro: string;
    total: number;
    estatus: number;
    activo: boolean;
    pagado: boolean;
    cancelado: boolean;
    creadoPor: number;
    remitente: number;
    destinatario: number;
    sucursal: number;
    destino: number;
}
