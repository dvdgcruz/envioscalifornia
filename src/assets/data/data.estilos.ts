export const ESTILOS = {
    headerEmpresa: {
        fontSize: 24,
        bold: true,
        alignment: 'center'
    },
    headerTop: {
        fontSize: 13,
        bold: true,
        alignment: 'center'
    },
    headerBold: {
        fontSize: 12,
        bold: true,
        alignment: 'center'
    },
    headerItalic: {
        fontSize: 12,
        italics: true,
        alignment: 'center'
    },
    subheader: {
        fontSize: 10,
        bold: false,
        fillColor: '#BDBDBD',
        color: 'black',
        alignment: 'center'
    },
    numeros: {
        alignment: 'center'
    },
    justificado: {
        fontSize: 10,
        alignment: 'justify',
        margin: [10, 5, 10, 1]
    },
    nota: {
        fontSize: 8,
        alignment: 'justify',
        margin: [35, 0, 40, 0],
    },
    normal: {
        fontSize: 12,
        margin: [25, 5, 25, 1]
    },
    tableExample: {
        margin: [25, 5, 25, 5]
    },
    tableHeader: {
        bold: true,
        fontSize: 10,
        color: 'black'
    },
    defaultStyle: {
        fontSize: 12,
        color: 'black'
    }
};
