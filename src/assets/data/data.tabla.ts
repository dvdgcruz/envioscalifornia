/*
████████  █████  ██████  ██       █████
   ██    ██   ██ ██   ██ ██      ██   ██
   ██    ███████ ██████  ██      ███████
   ██    ██   ██ ██   ██ ██      ██   ██
   ██    ██   ██ ██████  ███████ ██   ██
*/
Object.byString = (o, s) => {
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, ''); // strip a leading dot
    const a = s.split('.');
    for (let i = 0, n = a.length; i < n; ++i) {
        const k = a[i];
        if (k in o) {
            o = o[k];
        } else {
            return;
        }
    }
    return o;
};

// Table body builder
function buildTableBody(data, columns, showHeaders, headers) {
    const body = [];
    // Inserting headers
    if (showHeaders) {
        body.push(headers);
    }
    // Inserting items from external data array
    data.forEach( row => {
        const dataRow: any[] = [];
        let i = 0;
        columns.forEach( (column) => {
            dataRow.push({
                fontSize: 10,
                text: Object.byString(row, column),
                alignment: headers[i].alignmentChild
            });
            i++;
        } );
        body.push(dataRow);
    });
    return body;
}

// Func to return generated table
function table( data, columns, witdhsDef, showHeaders, headers, layoutDef ) {
    return {
        style: 'tableExample',
        table: {
            headerRows: 1,
            widths: witdhsDef,
            body: buildTableBody(data, columns, showHeaders, headers)
        },
        layout: layoutDef
    };
}

function soloLetras(e) {
    const key = e.keyCode || e.which;
    const tecla = String.fromCharCode(key).toLowerCase();
    const letras = 'áéíóúabcdefghijklmnñopqrstuvwxyz';
    const especiales = [8, 37, 39, 46];

    let teclaEspecial = false;
    for (const i in especiales) {
        if (key === especiales[i]) {
            teclaEspecial = true;
            break;
        }
    }

    if ( letras.indexOf( tecla ) === -1 && !teclaEspecial) {
        return false;
    }
}
function Numeros( cadena: string ) {// Solo numeros
    let out = '';
    const filtro = '1234567890'; // Caracteres validos

    // Recorrer el texto y verificar si el caracter se encuentra en la lista de validos
    for ( let i = 0; i < cadena.length; i++ ) {
        if ( filtro.indexOf( cadena.charAt(i) ) !== -1 ) {
            // Se añaden a la salida los caracteres validos
            out += cadena.charAt(i);
        }
    }

    // Retornar valor filtrado
    return out;
}
function mayus(e) {
    e.value = e.value.toUpperCase();
}
